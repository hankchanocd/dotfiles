# Command Prompt Config
PS1='-> '

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Path to git-extra 'zsh completions'
source /usr/local/opt/git-extras/share/git-extras/git-extras-completion.zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it will load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="mh"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="false"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=30

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(github git-flow osx sublime z zsh-autosuggestions zsh-better-npm-completion zsh-syntax-highlighting vagrant wakatime)

source $ZSH/oh-my-zsh.sh

# Direnv
eval "$(direnv hook zsh)"

# Set History size
HISTFILE=~/.zhistory
HISTSIZE=SAVEHIST=10000
setopt sharehistory
setopt extendedhistory

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
  export EDITOR='mvim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Configuration
alias ohmyzsh="subl ~/.oh-my-zsh"
alias loadrvm='[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"'
alias loadnvm='[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"'

############################################
# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# `source` the collection of scripts that are common to both zsh and bash
# `source` the collection of my custom scripts
for f in ~/.script/.*; do
  # Exclude directories, i.e. '.', '..'
  [ ! -d "$f" ] || continue
  source $f
done
############################################

# Only if fzf is installed
if type fzf >/dev/null 2>&1; then
  ### fzf auto-completion ###
  # Ref: https://github.com/junegunn/fzf/wiki/Configuring-fuzzy-completion
  [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

  # Fish like interactive tab completion for cd in zsh
  [ -f ~/.script/sh/zsh-interactive-cd.plugin.zsh ] && source ~/.script/sh/zsh-interactive-cd.plugin.zsh
fi

###-being-exercism-completion-###
if type compdef >/dev/null 2>&1; then
  if [ -f ~/.config/exercism/exercism_completion.zsh ]; then
    . ~/.config/exercism/exercism_completion.zsh
  fi
fi
###-end-exercism-completion-###

###-thefuck-commands-correction-###
eval $(thefuck --alias)

### Add FPATH for hub to work with the system zsh ###
if ((!${fpath[(I) / usr / local / share / zsh / site - functions]})); then
  FPATH=/usr/local/share/zsh/site-functions:$FPATH
fi

### Initialize jenv for managing the use of different JDK ###
eval "$(jenv init -)"

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _approximate
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall
autoload -Uz bashcompinit
bashcompinit

# pip zsh completion start
function _pip_completion() {
  local words cword
  read -Ac words
  read -cn cword
  reply=($(COMP_WORDS="$words[*]" \
    COMP_CWORD=$((cword - 1)) \
    PIP_AUTO_COMPLETE=1 $words[1]))
  }
compctl -K _pip_completion pip
# pip zsh completion end

# Google Cloud SDK
source '/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.zsh.inc'
source '/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.zsh.inc'

# tabtab source for AWS serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /usr/local/bin/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . /usr/local/bin/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /usr/local/bin/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . /usr/local/bin/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh
# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /usr/local/bin/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh ]] && . /usr/local/bin/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh

# doctl (Digital Ocean CLI) auto-completion
source <(doctl completion zsh)

# kubectl configurations
alias kb=kubectl
complete -F __start_kubectl kb

