# Anatomy

The project structure of Dotfiles. We are only listing items that haven't been listed on [README](./README.md).

## Shell environment

- .zshrc
- .bash_profile

p.s. Most scripts and aliases common to both `zsh` and `bash` environments are put together in `/.script/`. See below.

## [Scripts](https://gitlab.com/hankchanocd/dotfiles/tree/master/.script)

These scripts are exposed to the shell environment

- .aliases

  - `env`: `env` with `fzf`

  <img src="/images/env_demo.png"  width="550" height="458">

  - `music`: run [`mocp`](https://github.com/jonsafari/mocp) underneath

  <img src="/images/music_demo.png"  width="550" height="458">

- .cpu
- .exports
- .functions
- .javascript
- .network
- .pkg_manager
- .python
- .search

  - `cdf`: better `cd` with `fzf` as well as preview

  <img src="/images/cdf_demo.png"  width="550" height="458">

  - `cmd`: list all available commands with `fzf` (run `compgen` underneath)

  <img src="/images/cmd_demo.png"  width="550" height="458">

  - `path`: better `path` with `fzf`

  <img src="/images/path_demo.png"  width="550" height="458">

- .server
- .tasks

## Git

- .gitconfig

  - `git ov`: aliased to `git overview`

  - `git starring`: aliased to `starring --all`

- .gitignore_global
- .stCommitMsg

## Vim

- .vimrc

## [One-off scripts](https://gitlab.com/hankchanocd/dotfiles/tree/master/.script/bin)

1. These scripts aren't yet publicly distributed, used only internally.

- `ccat`

```
ccat is a collection of all file types' ccat technique.
It prints .json, .html, .css, .scss, .js, .csv, .py, .md, .pl

where:
	-h
	show this help text

	-j, --json
	Use jq to color json files

	-r, --rainbow
	rainbow-color the printout with lolcat

	-a, --all
	Print all files, and files only, in the current directory
```

- `imgcat`

- `monthlyUpdate`

- `upgrade`

- `overview`

- `git-blames`

  `git blame` with `fzf` and preview

  <img src="/images/git_blames_demo.png"  width="550" height="458">

- `git fuzzy-aliases`: aliased as `git aliases`

  Show a list of all available `git` aliases

- `git fuzzy-branch`: aliased as `git fb`

  Show a list of all available `git` branches

- `git fuzzy-commit`: aliased as `git fc`

  Show a list of commits with preview

- `git fuzzy-gitignore`: aliased as `git fg`

  Show the list from `.gitignore`

- `git fuzzy-gitignore-io`: aliased as `git fgio`

  Download the selected `.gitignore` file

- `git fuzzy-tag`: aliased as `git ft`

  Show a list of tags

- `git spark`

  Show ▁▂▄▁▅ based on commit frequency.

- `zstat`: better [`z`](https://github.com/rupa/z) with `fzf` and full statistics display

  <img src="/images/zstat_demo.png"  width="550" height="458">

<br>

2. These scripts are now publicly available as stand-alone repos

- [`git commands`](https://github.com/hankchanocd/git-commands): aliased as `git cmd`

  Fuzzy search `git` commands with `fzf`

- [`git-overview`](https://github.com/hankchanocd/git-overview)

  A quick `git` repository insight at terminal

- [`git-stack`](https://github.com/hankchanocd/git-stack/tree/master)

  Compare against `master` branch with `fzf`

## Configurations

- .highlightrc

## GUI apps configurations

- .hyper.js
