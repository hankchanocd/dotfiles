#!/usr/bin/env bash
#!/usr/bin/env zsh
# Capture terminal result on iTerm

screencapture -l$(osascript -e 'tell app "iTerm" to id of window 1') ~/test.png

sleep 1

open ~/test.png
