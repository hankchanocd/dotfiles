#!/usr/bin/env bash
#!/usr/bin/env zsh
#
# Smarter z with fzf
# z should show the most recent usage, in contrast to zstat's showing the overall usage
#
# Requirements: z, fzf, ack

# Note 1: `cd` won't work in a subshell created by the script.
# The script needs to be triggered by the `cd` call in function.
# See .functions

# Note 2: since script is executed in a subshell, z needs to be loaded again to make it
# available for main
# Load z.sh
source ~/.oh-my-zsh/plugins/z/z.sh

# Proceed to the prioritised dir given by z using fzf
function main() {
	# Color scheme used exclusively here
	FZF_DEFAULT_OPTS='
	--color hl:33,fg+:214,hl+:33
	--color spinner:208,pointer:196,marker:208'

	if type fzf >/dev/null 2>&1; then
		[ $# -gt 0 ] && _z "$*" && return

		if type ack >/dev/null 2>&1; then # Coloring
			echo "$(_z -l 2>&1 |
				sed 's/^[0-9,.]* *//' | # Remove score
				ack --color --color-match=bright_green --passthru '[^/ ]+$' |
				ack --color --color-match=bright_green --passthru '/' |
				fzf --ansi --cycle --height=40% --reverse --inline-info --no-sort --tac --query "${*##-* }")"
		else
			echo "$(_z -l 2>&1 |
				sed 's/^[0-9,.]* *//' | # Remove score
				fzf --ansi --cycle --height=40% --reverse --inline-info --no-sort --tac --query "${*##-* }")"
		fi

	else
		_z "$*"
	fi
}


################ Help ################
usage="usage: $(basename "$0") [<options>]

Smarter z with fzf

where:
	-h  show this help text"

############# Parse options ##############
while getopts ':h' option; do
	case "$option" in
	h)
		echo "$usage"
		exit 0
		;;
	:)
		main
		exit
		;;
	\?)
		printf "illegal option: -%s\n" "$OPTARG" >&2
		echo -e "$usage" >&2
		exit 1
		;;
	esac
done
shift $((OPTIND - 1))

# Default case when no option provided
main
