#!/usr/bin/env bash
#!/usr/bin/env zsh
# backuprc is used to back up scripts to the Dotfiles folder

################################
####### Helper functions #######
cp_recursive() {
	if type rsync >/dev/null 2>&1; then # use rsync if rsync exists
		rsync -a --stats --human-readable "$1" "$2"
	else # default to `cp -r`
		cp --verbose -r "$1" "$2"
	fi
}

print_title() {
	echo -e "\e[33m$1\e[39m" # yellow followed by white
}

print_error() {
	echo -e "\e[91m$1\e[39m" # red followed by white
}

line() {
	if type hr >/dev/null 2>&1; then # use hr if hr exists
		hr "$1"
	else
		multi="$1$1$1$1$1$1$1$1$1"
		print_title "$multi$multi$multi$multi$multi$multi$multi"
	fi
}

###########################
####### preprocess ########
# Path to Dotfiles' root directory
# Set DIR, and abort the script if errored
dotfiles=$1
preprocess() {
	if [[ -z "$dotfiles" ]]; then
		print_error "\e[91m No path is given, abort \e[39m"
		exit 1
	elif [[ ! -d "$dotfiles" ]]; then
		print_error "\e[91m Path does not exist, abort \e[39m"
		exit 1
	fi
}

###########################
##### main operations #####
copy_dotfiles() {
	print_title "Copy dotfiles..."

	cp -v ~/.zshrc $dotfiles
	cp -v ~/.bash_profile $dotfiles
	cp -v ~/.vimrc $dotfiles
	cp -v ~/.sshrc $dotfiles
	cp -v ~/.czrc $dotfiles

	# Copy .gitconfig
	cp -v ~/.gitconfig $dotfiles
	cp -v ~/.gitignore_global $dotfiles

	# Copy hyper rc
	cp -v ~/.hyper.js $dotfiles

	# Copy fzf config files
	cp -v ~/.fzf.bash $dotfiles
	cp -v ~/.fzf.zsh $dotfiles
	line ' '

	# Copy .highlightrc dir
	print_title "Copy highlightrc files..."
	cp_recursive ~/.highlightrc $dotfiles
	line '*'
	line ' '
}

# Copy .tasks .archive .search .javascript in the .script folder
copy_scripts() {
	print_title "Copy scripts..."
	cp_recursive ~/.script $dotfiles
	line ' '
}

####################
main() {
	preprocess

	print_title "Back up files to"
	print_title "$dotfiles ..."
	line ' '

	copy_dotfiles
	copy_scripts

	echo -e "\e[92mComplete" # light green
	cd $dotfiles
}

main "$@"
