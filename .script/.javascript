#!/usr/bin/env bash
# .javascript encompasses a variety of JavaScript cli tools

function babelsh() { # Shortcut for babel
  if [ "$1" = "--output" ] || [ "$1" = "-o" ]; then
    babel "$2" --out-file "compiled-$2"
    js-beautify --replace "compiled-$2"
  elif [ "$1" = "build" ]; then
    npm run build # See 'script' of the local package.json
  else
    npx babel "$@" | pygmentize -f terminal256 -O style=native -g # Color cat output using pythons pygmentize
  fi
}

function uglify() { # Shortcut for uglify-js (for JS) and pyminifier (for Python)
  # Javascript
  if [[ "$@" == *".js" ]]; then
    uglifyjs "$@" --output "minify-$@"
    echo It is now uglified to "minify-$@"

    # Python
  elif [[ "$@" == *".py" ]]; then
    pyminifier --obfuscate "$@" >"minify-$@"
    echo It is now minified and obfuscated to "minify-$@"

    # Everything else
  else
    uglifyjs "$@" --output "minify-$@"
  fi
}

function browserifysh() { # Shortcut for browserify
  browserify "$1" >"bundled-$1"
}

function beautify() {
  if [ "$1" = "-h" ] || [ "$1" = "--html" ]; then
    js-beautify "$2" | highlight --theme ~/.highlightrc/.highlight_html_rc
  elif [[ "$@" == *".html" ]]; then
    js-beautify "$@" | highlight --theme ~/.highlightrc/.highlight_html_rc
  else
    js-beautify "$@" | highlight --theme ~/.highlightrc/.highlight_js_rc
  fi
}
