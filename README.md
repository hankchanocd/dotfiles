# Hank's Dotfiles

🏠 Opinionated configuration files for the ultimate development environment (Mac OS X)

- I maintain this repo as my dotfiles, but I have put in extra efforts to make it easily reproducible on other machines.
- You are welcome to make suggestions (i.e. point out typos), but I may decline if the suggestion falls far outside my current tool set.
- I started off this repo from the [**Holy Grail of Dotfiles**](https://dotfiles.github.io), and am still able to learn new stuffs from there.

## Setup

### Install

Once you clone this repository,

- **Beginners** (only those who haven't yet configured their dotfiles):

  `cd` into this repository and run `./init` to initiate the transferring process. This process will replace your existing dotfiles at your home directory and create them if not yet existent. You may choose to delete this repository afterwards.

  - **Veterans** (as long as you have modified your dotfiles):

    1. (optional) **Create `local` Branch**

    If you wish to receive updates from this repo, it's better you have a designated branch for configuring changes from updates. Check out a new `git` branch named `local` (or anything else you like). Use `master` branch to pull updates, and merge new changes to `local` branch, where you will deal with merge conflicts between updates and your own dotfiles. Skip this step if you wish to only use this repo once and delete it afterwards.

    p.s. This practice is also recommended for managing your own dotfiles repo across multiple machines.

    2. **Backup**

    Run `./backuprc` to back up your own dotfiles to this repo. It's highly recommended to modify your dotfiles here, so you won't accidentally mess up your own dotfiles at your home directory, which is likely not backed up by `git`. You can also use `git diff` to give you a green-red-contrast view of the changes between your dotfiles and this repo's.

    3. **Run `init`**

    After you are done with changes, run `./init` to initiate the transferring process. This process will replace your existing dotfiles at your home directory and create them if not yet existent.

### Requirements

A Mac machine (Duh~), preferably new.

(Optional) This repo recommends a few GUI apps for programming. Though optional, most of the dotfiles here are optimized for these apps.

- iTerm2
- Hyper
- Visual Studio Code
- GitKraken

(Required) You need to download the following programs from [**homebrew**](https://brew.sh), [**pip**](https://pip.pypa.io/en/stable/), [**gem**](https://rubygems.org/pages/download), and [**npm**](https://docs.npmjs.com/getting-started/what-is-npm) to ensure the custom commands from this repo are running properly. These programs don't have to be downloaded all at once, but it's highly recommended. Without these installs some commands might still work by defaulting to the native MacOS commands, while some others are out of luck.

- `brew`:

  - `ag`
  - `fd`
  - `fzf`
  - `grc`
  - `hr`
  - `imagemagick`
  - `jq`
  - `lazygit`
  - `ripgrep`
  - `rsync`
  - `wifi-password`
  - `z` (or install it from [`oh-my-zsh`](https://github.com/robbyrussell/oh-my-zsh) if you are using `zsh`)

- `pip` (or use `pip3` if not available in `pip`)

  - `termtosvg`

- `npm --global`

  - `cli-highlight`

  - `starring`

### Use

Simply start a new shell, and those installed scripts will be loaded into your shell.

### Maintain

(optional) If you wish to use this repo to maintain all your dotfiles from now on. Use `backuprc`, which is loaded in your shell, to copy all your dotfiles over to this repo, and commit/push from `local` branch to your remote repo as you like.

```bash
$ backuprc
[~/Path/To/Repo]$ git add . && git commit
[~/Path/To/Repo]$ git push
```

### Receive Updates

(optional) If you wish to receive updates from this repo, simply use `master` branch to pull changes, and use `local` branch for merging the changes into your dotfiles. Run `./init` again to transfer the files over to your home directory.

```bash
(master)$ git pull origin master
(master)$ git checkout local
(local)$ git merge master
(local)$ ./init
```

## Anatomy

[Anatomy](./ANATOMY.md).

## tl;dr

There are many gems in every corner of this repo, but below are my favorites, many of which are upgraded with [`fzf`](https://github.com/junegunn/fzf).

p.s. Demo is done on zsh. Color might be different on bash.

- [.search](./.script/.search): list outputs using `fzf`

  - `aliases`: `alias` with `fzf`

  <img src="/images/aliases_demo.png"  width="550" height="458">

  - `cd`: better `cd` with `fzf`

  <img src="/images/cd_demo.png"  width="550" height="458">

  - `cmd`: list all available commands with `fzf` (run `compgen` underneath)

  - `f`: shortcut to the custom `fzf` with preview mode.

  <img src="/images/f_demo.png"  width="550" height="458">

  - `fopen`: open file with VS Code

  - `vopen`: open file with `vim`

- [.aliases](./.aliases)

  - `back`: go to the previous directory

  - `env`: `env` with `fzf`

  - `termcapture`: screenshot a terminal window. (run `screencapture` underneath)

  - `termrecord`: record a defined terminal activity. (run [`termtosvg`](https://github.com/nbedos/termtosvg) underneath)

- [.vimrc](./.vimrc)

- [.cpu](./.script/.cpu)

  - `pss`: better `ps` with `fzf`

  <img src="/images/pss_demo.png"  width="550" height="458">

- [.functions](./.script/.functions)

  - `backuprc`: back up your dotfiles to this repo

  - `pwdcopy`: copy `pwd` output to clipboard

  - `up`: go to the parent directory by the given level

- [.pkg_manager](./.script/.pkg_manager)

  - `nps`: search npm module with `npms` underneath

- [.gitconfig](./.gitconfig)

  - `git aliases`: `git alias` with `fzf`

  <img src="/images/git_aliases_demo.png"  width="550" height="458">

  - `git cmd`: show help for all the available git sub-commands

  <img src="/images/git_cmd_demo.png"  width="550" height="458">

  - `git fc`: `git log` with preview using `fzf`

  <img src="/images/git_fc_demo.png"  width="550" height="458">

  - `git ft`: `git tag` with preview using `fzf`

- [bin](https://gitlab.com/hankchanocd/dotfiles/tree/master/.script/bin)

  - `ccat`: `cat` with syntax highlights (run `cli-highlight` underneath)

  - [`git-overview`](https://github.com/hankchanocd/git-overview): A quick git repository insight at terminal

  - `z`: better [`z`](https://github.com/rupa/z) with `fzf`

  <img src="/images/z_demo.png"  width="550" height="458">

<br/>

Discover more useful commands and aliases at [Anatomy](./ANATOMY.md).

## Contribution

Like stated in the beginning, this repository is more of a personal project. If you would still like to help out, here are a few suggestions:

- [`shfmt`](https://github.com/mvdan/sh) does a great job at formatting shell scripts. Even better, Visual Studio Code has the [shell-format](https://marketplace.visualstudio.com/items?itemName=foxundermoon.shell-format) plugin that leverages on `shfmt`

Please see [here](./CONTRIBUTION.md) on how to contribute.

## Changelog

[Changelog](./CHANGELOG.md)

## License

[MIT](./LICENSE)
