" References
" Reference (1): http://vimuniversity.com/samples/your-first-vimrc-should-be-nearly-empty
" Reference (2): https://github.com/jez/vim-as-an-ide/commit/dff7da3"

" Original Author: Hank Chan "
" Last change: 2018 Jun 13 "


"                     -- Use Vim settings, rather than Vi settings.-- "
" This must be first, because it changes other options as a side effect "
set nocompatible

" Make backspace behave in a sane manner "
set backspace=indent,eol,start
set ruler
set showcmd
set incsearch
set hlsearch
set history=1000
set linebreak
set scrolloff=3

" Indent wrapped lines up to the same level
if exists('&breakindent')
  set breakindent
endif

" Tab settings
set expandtab  "Expand tabs into spaces
set tabstop=2  "default to 2 spaces for a hard tab
set softtabstop=2  "default to 2 spaces for the soft tab
set shiftwidth=2  "for when <TAB> is pressed at the beginning of a line

" Switch syntax highlighting on "
syntax on

" Enable file type detection and do language-dependent indenting. "
filetype plugin indent on

" Show line numbers "
set number

" Allow hidden buffers, don't limit to 1 file per window/split "
set hidden

" Enable mouse access "
set mouse=a

" Design Preference
set background=dark

" For plugins like Syntastic and vim-gitgutter which put symbols
hi clear SignColumn

"                               -- Vundle Config --
filetype off

" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-easytags'
Plugin 'majutsushi/tagbar'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'vim-scripts/align'
Plugin 'Chiel92/vim-autoformat'
Plugin 'vim-scripts/HTML-AutoCloseTag'
"                               -- Alternate.vim (:AT to open up the alternate file)
Plugin 'vim-scripts/a.vim'
"                               -- Working with Git --
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
"                               -- Other text editing features --
Plugin 'Raimondi/delimitMate'
"                               -- Making Vim look good --
Plugin 'altercation/vim-colors-solarized'
Plugin 'tomasr/molokai'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
"                               -- Vim as a programmer's text editor --
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'wakatime/vim-wakatime'
"                               -- Syntax Highlighting --
Plugin 'ekalinin/Dockerfile.vim'
Plugin 'keith/swift.vim'
Plugin 'digitaltoad/vim-jade'
Plugin 'tpope/vim-liquid'
Plugin 'cakebaker/scss-syntax.vim'
Plugin 'leafgarland/typescript-vim'

" Enable swift with syntastic
let g:syntastic_swift_checkers = ['swiftpm', 'swiftlint']

" All of the plugins must be added before the following line
call vundle#end()

filetype plugin indent on

" We need this for plugins like Syntastic and vim-gitgutter which put symbols in the sign column.
hi clear SignColumn



"                               -- Plugin-Specific Settings --
"                               -- altercation/vim-colors-solarized settings --
" Toggle this to "light" for light colorscheme
set background=dark

" Uncomment the next line if your terminal is not configured for solarized
let g:solarized_termcolors=256

" Set the colorscheme
colorscheme solarized


"                               -- ntpeters/vim-better-whitespace --
" Strip trailing white space by default
let g:strip_whitespace_on_save = 1


"                               -- Chiel92/vim-autoformat --
" A shortcut(Shift-a) to autoformat
map <S-a> :Autoformat<CR>

" Have code formatted upon saving file
au BufWrite * :Autoformat

"                               -- bling/vim-airline settings --
" Always show statusbar
set laststatus=2

" Fancy arrow symbols, requires a patched font
" To install a patched font, run over to
"     https://github.com/abertsch/Menlo-for-Powerline
" download all the .ttf files, double-click on them and click "Install"
" Finally, uncomment the next line
"let g:airline_powerline_fonts = 1

" Show PASTE if in paste mode
let g:airline_detect_paste=1

" Show airline for tabs too
let g:airline#extensions#tabline#enabled = 1

" Use the solarized theme for the Airline status bar
let g:airline_theme='solarized'


"                               -- xolox/vim-easytags settings --
" - vim-easytags is a plugin that generates tags files, which is a compiled
"   index of all the functions, variables, and identifies that you use in your
"   project
" - vim-misc is a dependency of vim-easytags

" Where to look for tag files
set tags=./tags;,~/.vimtags
" Sensible defaults
let g:easytags_events = ['BufReadPost', 'BufWritePost']
let g:easytags_async = 1
let g:easytags_dynamic_files = 2
let g:easytags_resolve_links = 1
let g:easytags_suppress_ctags_warning = 1

"                               -- majutsushi/tagbar settings --
" - tagbar is a plugin that reads the output from ctags and displays the
"   information in an accessible way inside Vim

"" Open/close tagbar with \b
nmap <silent> <leader>b :TagbarToggle<CR>

" A shortcut(Shift-B) to toggle tagbar
map <S-b> :TagbarToggle<CR>

" Uncomment to open tagbar automatically whenever possible
"autocmd BufEnter * nested :call tagbar#autoopen(0)


"                               -- airblade/vim-gitgutter settings --
" In vim-airline, only display "hunks" if the diff is non-zero
let g:airline#extensions#hunks#non_zero_only = 1


"                               -- Raimondi/delimitMate settings --
let delimitMate_expand_cr = 1
augroup mydelimitMate
  au!
  au FileType markdown let b:delimitMate_nesting_quotes = ["`"]
  au FileType tex let b:delimitMate_quotes = ""
  au FileType tex let b:delimitMate_matchpairs = "(:),[:],{:},`:'"
  au FileType python let b:delimitMate_nesting_quotes = ['"', "'"]
augroup END


"                               -- jistr/vim-nerdtree-tabs settings --
" Open a NERDTree automatically when vim starts up "
autocmd vimenter * NERDTree

" Open a NERDTree automatically when vim starts up if no files specified "
autocmd StdinReadPre * let s:std_in=1
autocmd vimenter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Open a NERDTree automatically when vim starts up on opening a directory "
autocmd StdinReadPre * let s:std_in=1
autocmd vimenter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

" Open/Close NERDTree Tabs with \t
nmap <silent> <leader>t :NERDTreeTabsToggle<CR>

" A shortcut(Shift-Q) to toggle NERDTree
map <S-q> :NERDTreeToggle<CR>

" Close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


"                               -- scrooloose/syntastic settings --
" Syntastic gives you error checking for just about every language imaginable
" right from within vim. It shows you the offending line next to the line
" numbers.
let g:syntastic_error_symbol = '✘'
let g:syntastic_warning_symbol = "▲"
augroup mySyntastic
  au!
  au FileType tex let b:syntastic_mode = "passive"
augroup END


" Set runtime path for CrtlP & NERDTree & Ack
set runtimepath^=~/.vim/bundle/ctrlp.vim
set runtimepath+=~/.vim/bundle/nerdtree
set runtimepath+=~/.vim/bundle/ack.vim


" Use ag search "
let g:ackprg = 'ag --nogroup --nocolor --column'


" Use fzf auto-suggestions "
set rtp+=/usr/local/opt/fzf


" Custom commands to move among tabs in Konsole-style
nnoremap <C-A-Left> :tabprevious<CR>
nnoremap <C-A-Right> :tabnext<CR>
" Custom commands to close tabs by deleting buffer
nnoremap <C-w> :bd<CR>
